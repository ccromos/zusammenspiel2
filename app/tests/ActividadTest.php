<?php

class ActividadTest extends TestCase {

        
    public function testActividadesVigentes() {
        $actividades = Actividad::all();
        $numActValidos=0;
        foreach ($actividades as $actividad) {
            $fechActu=date("Y-m-d");
            $fecActi=$actividad->fecha;
            $dias=(strtotime($fecActi) - strtotime($fechActu))/86400;
            
            if($dias==7)
                $numActValidos++;
            
        }$this->assertEquals($numActValidos,1);
    }
    public function testActividadesPasadas(){
        $actividades=Actividad::all();
        $vencidos=0;
        $fechActu=date("Y-m-d");
        foreach ($actividades as $actividad){
            $fechaActiv=$actividad->fecha;
            if ($fechaActiv<$fechActu)
                   $vencidos++;
            
    }
    $this->assertEquals(2,$vencidos);
    
     }                                                                                                                                                                                                                                                                                                                                                                                                                                
}