<?php

class PlanGlobalTest extends TestCase {

    public function testUrlValido() {
        $crawler = $this->client->request('GET', '/plan_global');
        $this->assertTrue($this->client->getResponse()->isOk());
    }

    public function testNombreAsignaturaValido() {
        $planes_globales = PlanGlobal::all();
        foreach ($planes_globales as $plan_global) {
            $this->assertTrue(strlen($plan_global->asignatura) >= 5);
        }
    }

    public function testNombreDocenteValido() {
        $planes_globales = PlanGlobal::all();
        $patron = "/^[a-zA-Z ]+$/";
        foreach ($planes_globales as $plan_global) {
            $this->assertNotEmpty($plan_global->docente);
            $this->assertTrue(preg_match($patron, $plan_global->docente) == 1);
        }
    }

    public function testCodigoSisMateriaValido() {
        $planes_globales = PlanGlobal::all();
        $patron = "/^[[:digit:]]+$/";
        foreach ($planes_globales as $plan_global) {
            $this->assertNotEmpty($plan_global->codigo_sis);
            $this->assertTrue(preg_match($patron, $plan_global->codigo_sis) == 1);
        }
    }

    public function testGestionValido() {
        $planes_globales = PlanGlobal::all();
        $patron = "/^[[:digit:]]-[[:digit:]][[:digit:]][[:digit:]][[:digit:]]$/";
        foreach ($planes_globales as $plan_global) {
            $this->assertNotEmpty($plan_global->gestion);
            $this->assertTrue(preg_match($patron, $plan_global->gestion) == 1);
        }
    }

    public function testGestionPrimerValorValido() {
        $planes_globales = PlanGlobal::all();
        foreach ($planes_globales as $plan_global) {
            $primerValor = explode('-', $plan_global->gestion);
            $primerValor = $primerValor[0];
            $result = $primerValor == '1' ||
                    $primerValor == '2' ||
                    $primerValor == '3' ||
                    $primerValor == '4';
            $this->assertTrue($result);
        }
    }

    public function testTemaIntroduccionObligatorio() {
        $planes_globales = PlanGlobal::all();
        foreach ($planes_globales as $plan_global) {
            $temas = $plan_global->temas;
            $res = false;
            foreach ($temas as $tema) {
                $res = $res || $tema->nombre == 'Introduccion';
            }
            $this->assertTrue($res);
        }
    }

    public function testBibliografiaValida() {
        $planes_globales = PlanGlobal::all();
        foreach ($planes_globales as $plan_global) {
            $bibliografias = $plan_global->bibliografias;
            $this->assertTrue(count($bibliografias) >= 1);
        }
    }

}
