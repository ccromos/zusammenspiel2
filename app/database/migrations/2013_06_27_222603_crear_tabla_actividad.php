<?php

use Illuminate\Database\Migrations\Migration;

class CrearTablaActividad extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            //CREACION TABLA COLUMNA
                Schema::table('actividad', function($table) {
                    $table->create();
                    $table->increments('id');                    
                    $table->string('titulo');
                    $table->string('descripcion');
                    $table->date('fecha');
                });               
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::drop('actividad');
	}

}