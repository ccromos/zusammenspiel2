<?php

use Illuminate\Database\Migrations\Migration;

class CrearTablaRespuesta extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('respuesta', function($table) {
                    $table->create();
                    $table->increments('id');
                    $table->integer('usuario_id');
                    $table->integer('pregunta_id');
                    $table->string('descripcion');
                    $table->date('fecha_publicacion');
                });
        Schema::table('respuesta', function($table) {
                    $table->dropPrimary('respuesta_id_primary');
                });
        Schema::table('respuesta', function($table) {
                    $table->primary(array('id', 'pregunta_id'));
                    $table->foreign('usuario_id')->references('id')->on('usuario')->onDelete('cascade');
                    $table->foreign('pregunta_id')->references('id')->on('pregunta')->onDelete('cascade');
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('respuesta');
    }

}