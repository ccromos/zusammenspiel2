<?php

use Illuminate\Database\Migrations\Migration;

class AgregarFilaTablaPlanGlobal extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::table('plan_global')->insert(
            array(
                array(
                    'asignatura'=>'Arquitectura de Software',    
                    'codigo_sis'=>'2010100',    
                    'departamento'=>'Informatica y Sistemas',    
                    'docente'=>'Pablo Azero Alcocer',    
                    'gestion'=>'2-2005',    
        )));
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::table('plan_global')->delete();
    }

}