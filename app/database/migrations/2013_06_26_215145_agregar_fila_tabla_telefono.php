<?php

use Illuminate\Database\Migrations\Migration;

class AgregarFilaTablaTelefono extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::table('telefono')->insert(
                array(
                    array ('usuario_id'=>'1','numero'=>'1234567'),
                    array ('usuario_id'=>'1','numero'=>'1234567'),
                    array ('usuario_id'=>'1','numero'=>'1234567'),
                    array ('usuario_id'=>'2','numero'=>'1234567'),
                    array ('usuario_id'=>'8','numero'=>'1234567'),
                    array ('usuario_id'=>'4','numero'=>'1234567')
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::table('telefono')->delete();
    }

}