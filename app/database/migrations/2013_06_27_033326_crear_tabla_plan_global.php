<?php

use Illuminate\Database\Migrations\Migration;

class CrearTablaPlanGlobal extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('plan_global', function ($table) {
            $table->create();
            $table->increments('id');
            $table->string('asignatura');
            $table->string('codigo_sis');
            $table->string('docente');
            $table->string('departamento');
            $table->string('gestion');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('plan_global');
    }

}