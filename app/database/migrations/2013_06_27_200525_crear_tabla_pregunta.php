<?php

use Illuminate\Database\Migrations\Migration;

class CrearTablaPregunta extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('pregunta', function($table) {
                    $table->create();
                    $table->increments('id');
                    $table->integer('usuario_id');
                    $table->string('titulo');
                    $table->string('descripcion');
                    $table->date('fecha_publicacion');
                });
        Schema::table('pregunta', function($table) {
                    $table->foreign('usuario_id')->references('id')->on('usuario')->onDelete('cascade');
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('pregunta');
    }

}