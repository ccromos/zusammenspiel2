<?php

use Illuminate\Database\Migrations\Migration;

class AgregarFilaTablaRespuesta extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::table('respuesta')->insert(
                array(
                    array('usuario_id' => '3',
                        'descripcion' => 'liderar el equipo ',
                        'fecha_publicacion' => '2013-06-24',
                        'pregunta_id' => '1'),
                    array('usuario_id' => '8',
                        'descripcion' => 'colaborar, motivar y ensenar al equipo en todo lo que sea necesario',
                        'fecha_publicacion' => '2013-06-25',
                        'pregunta_id' => '1')
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::table('respuesta')->delete();
    }

}