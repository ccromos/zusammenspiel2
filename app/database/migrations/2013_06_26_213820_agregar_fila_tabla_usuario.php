<?php

use Illuminate\Database\Migrations\Migration;

class AgregarFilaTablaUsuario extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::table('usuario')->insert(
                array(
                    array('nombre' => 'juan pablo', 'password' => 'asdf'),
                    array('nombre' => 'naira', 'password' => 'asdf'),
                    array('nombre' => 'vlady', 'password' => 'asdf'),
                    array('nombre' => 'thais', 'password' => 'asdf'),
                    array('nombre' => 'rosario', 'password' => 'asdf'),
                    array('nombre' => 'crhystian', 'password' => 'asdf'),
                    array('nombre' => 'franklin', 'password' => 'asdf'),
                    array('nombre' => 'elvis', 'password' => 'asdf')
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::table('usuario')->delete();
    }

}