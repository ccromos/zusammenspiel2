<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CrearTablaTema extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('tema', function($table) {
                    $table->create();
                    $table->increments('id');
                    $table->string('nombre');
                    $table->integer('plan_global_id');
                });
        Schema::table('tema', function($table) {
                    $table->dropPrimary('tema_id_primary');
                });
        Schema::table('tema', function($table) {
                    $table->primary(array('id', 'plan_global_id'));
                    $table->foreign('plan_global_id')->references('id')->on('plan_global')->onDelete('cascade');
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('tema');
    }

}
