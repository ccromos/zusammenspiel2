<?php

use Illuminate\Database\Migrations\Migration;

class AgregarFilaTablaActividad extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            DB::table('actividad')->insert(
                array(
                    array('titulo' => 'Feriado Nacional',
                        'descripcion' => 'Anio nuevo ... ',
                        'fecha' => '2013-01-01'),                        
                    array('titulo' => 'Matricula Verano',
                        'descripcion' => 'Venta de Matricula para la gestion 3 2012 ',
                        'fecha' => '2013-01-07'),
                    array('titulo' => 'Dia de Vlady',
                        'descripcion' => 'Dia de la vagancia para lagartear',
                        'fecha' => '2013-07-05'
                    )
        ));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            DB::table('actividad')->delete();
	}

}