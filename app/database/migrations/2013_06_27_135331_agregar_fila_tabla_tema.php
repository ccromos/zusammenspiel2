<?php

use Illuminate\Database\Migrations\Migration;

class AgregarFilaTablaTema extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::table('tema')->insert(
            array(
                array('nombre'=>'Introduccion','plan_global_id'=>'1'),    
                array('nombre'=>'Fundamentos de diseno','plan_global_id'=>'1'),    
                array('nombre'=>'Diseno guiado por pruebas','plan_global_id'=>'1'),    
                array('nombre'=>'Patrones de diseno','plan_global_id'=>'1'),    
                array('nombre'=>'Refactorizacion','plan_global_id'=>'1')   
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::table('tema')->delete();
    }

}