<?php

use Illuminate\Database\Migrations\Migration;

class AgregarFilaTablaPregunta extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::table('pregunta')->insert(
                array(
                    array('usuario_id' => '1', 
                          'titulo' => 'Scrum',
                          'descripcion' => 'Que funcion cumple el scrum master',
                          'fecha_publicacion'=>'2013-06-23'),
                  array('usuario_id' => '2', 
                          'titulo' => 'XP',
                          'descripcion' => 'Que es XP',
                          'fecha_publicacion'=>'2013-06-24')
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::table('pregunta')->delete();
    }

}