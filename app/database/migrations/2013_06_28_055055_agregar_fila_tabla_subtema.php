<?php

use Illuminate\Database\Migrations\Migration;

class AgregarFilaTablaSubtema extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::table('subtema')->insert(
                array(
                    array('nombre' => 'El paradigma orientado a objetos', 'tema_id' => '1'),
                    array('nombre' => 'Clase, Objeto', 'tema_id' => '1'),
                    array('nombre' => 'Herencia', 'tema_id' => '1'),
                    array('nombre' => 'Polimorfismo', 'tema_id' => '1'),
                    array('nombre' => 'Elemntos de diseno orientado a objetos', 'tema_id' => '1'),
                    array('nombre' => 'Encapsulamiento', 'tema_id' => '2'),
                    array('nombre' => 'Cohesion', 'tema_id' => '2'),
                    array('nombre' => 'Acoplamiento', 'tema_id' => '2'),
                    array('nombre' => 'Contratos y comportamiento', 'tema_id' => '2'),
                    array('nombre' => 'Ciclo de desarrollo', 'tema_id' => '3'),
                    array('nombre' => 'Estrategias de desarrollo del diseno orientado por pruebas', 'tema_id' => '3'),
                    array('nombre' => 'Infraestructura de pruebas : xunit', 'tema_id' => '3'),
                    array('nombre' => 'Concepto de patron', 'tema_id' => '4'),
                    array('nombre' => 'Caracterizacion de los patrones de diseno', 'tema_id' => '4'),
                    array('nombre' => 'Ejemplos de patrones', 'tema_id' => '4'),
                    array('nombre' => 'El patron arquitectonico MVC (Modelo Vista Controlador)', 'tema_id' => '4'),
                    array('nombre' => 'Evolucion de software', 'tema_id' => '5'),
                    array('nombre' => 'Transformaciones de programas', 'tema_id' => '5'),
                    array('nombre' => 'Malos olores', 'tema_id' => '5'),
                    array('nombre' => 'Refactorizacion', 'tema_id' => '5'),
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::table('subtema')->delete();
    }

}