<?php

use Illuminate\Database\Migrations\Migration;

class CrearTablaBibliografia extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('bibliografia', function($table) {
                    $table->create();
                    $table->increments('id');
                    $table->string('titulo_libro');
                    $table->string('autor');
                    $table->string('edicion');
                    $table->integer('plan_global_id');
                });
        Schema::table('bibliografia', function($table) {
                    $table->dropPrimary('bibliografia_id_primary');
                });
        Schema::table('bibliografia', function($table) {
                    $table->primary(array('id', 'plan_global_id'));
                    $table->foreign('plan_global_id')->references('id')->on('plan_global')->onDelete('cascade');
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('bibliografia');
    }

}