<?php

use Illuminate\Database\Migrations\Migration;

class AgregarFilaTablaBibliografia extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::table('bibliografia')->insert(
                array(
                    array('titulo_libro' => 'Patrones de diseno', 
                           'autor' => 'Erick Gamma',
                           'edicion' => 'Addison - Wesley',
                            'plan_global_id' => '1'),
                    array('titulo_libro' => 'PHP in action : Object, Desing, Agility', 
                           'autor' => 'Marcus Backer',
                           'edicion' => '2007',
                            'plan_global_id' => '1'),
                    array('titulo_libro' => 'Construccion de software orientado a objetos', 
                           'autor' => 'B. Meyer',
                           'edicion' => 'Prentice Hall',
                            'plan_global_id' => '1')
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::table('bibliografia')->delete();
    }

}