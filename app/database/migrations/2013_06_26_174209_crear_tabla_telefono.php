<?php

use Illuminate\Database\Migrations\Migration;

class CrearTablaTelefono extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('telefono', function($table) {
                    $table->create();
                    $table->increments('id');
                    $table->string('numero');
                    $table->integer('usuario_id');
                });
        Schema::table('telefono', function($table) {
                    $table->dropPrimary('telefono_id_primary');
                });
        Schema::table('telefono', function($table) {
                    $table->primary(array('id', 'usuario_id'));
                    $table->foreign('usuario_id')->references('id')->on('usuario')->onDelete('cascade');
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('telefono');
    }

}