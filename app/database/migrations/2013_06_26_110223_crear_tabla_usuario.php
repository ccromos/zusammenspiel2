<?php

use Illuminate\Database\Migrations\Migration;

class CrearTablaUsuario extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('usuario', function($table) {
                    $table->create();
                    $table->increments('id');
                    $table->string('nombre');
                    $table->string('password');
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('usuario');
    }

}