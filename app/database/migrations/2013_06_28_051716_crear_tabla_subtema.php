<?php

use Illuminate\Database\Migrations\Migration;

class CrearTablaSubtema extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('subtema', function($table) {
                    $table->create();
                    $table->increments('id');
                    $table->string('nombre');
                    $table->integer('tema_id');
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('subtema');
    }

}