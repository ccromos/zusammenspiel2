<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

Route::get('/', function() { return View::make('home.index'); });

Route::controller('/plan_global', 'PlanGlobalController');
Route::controller('/usuario', 'UsuarioController');
Route::controller('/foro', 'ForoController');
Route::controller('/calendario_actividades', 'ActividadController');
