<?php

class ForoController extends BaseController {
    public function getIndex() {
        $preguntas = Pregunta::all();
        return View::make('foro.index')->with('preguntas', $preguntas);
    }
	
    public function getPregunta($id) {
        $pregunta = Pregunta::find($id);
        if(is_null($pregunta)) {
            return Redirect::to('foro');
        }
        return View::make('pregunta.index')->with('pregunta',$pregunta);
    }
	
	public function getCrearPregunta(){
		return View::make('pregunta.crear');
	}
	public function postCrearPregunta(){
		$pregunta = new Pregunta();
		$pregunta->titulo = Input::get('titulo');
		$pregunta->usuario_id = Input::get('nombre');
		$pregunta->descripcion = Input::get('descripcion');
		$pregunta->fecha_publicacion= 'now()';
		$pregunta->save();
		return Redirect::to('foro');
	}
	public function getCrearRespuesta(){
		return View::make('respuesta.crear');
	}
	public function postCrearRespuesta(){
		$pregunta = new Respuesta();
		$pregunta->usuario_id = Input::get('usuario_id');
		$pregunta->pregunta_id = Input::get('pregunta_id');
		$pregunta->descripcion = Input::get('descripcion');
		$pregunta->fecha_publicacion= 'now()';
		$pregunta->save();
		return Redirect::to('/foro');
	}
}
