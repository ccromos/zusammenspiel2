<?php
class ActividadController extends BaseController {

    public function getIndex() {
        $actividades = Actividad::all();
        return View::make('actividad.index')->with('actividades', $actividades);
    }
    
 public function getRecordatorio() {
        $actividades = Actividad::all();
        $fechaActual = date("Y-m-d");
        //$fechaActual = "2013-01-14";
        $res = array();
        $index=0;
        foreach ($actividades as $actividad) {            
            $fechaActividad = $actividad->fecha;
            
            
            if ((strtotime($fechaActividad) - strtotime($fechaActual))/86400 == 7) {
                $index = $index+0;
                $res[$index] = $actividad;
                $index++;
            }
        }
        
        return View::make('actividad.recordatorio')->with('actividades', $res);
    }
}