<?php

class PlanGlobalController extends BaseController {

    public function getIndex() {
        $plan_global = PlanGlobal::all()->first();
        return View::make('planglobal.index')->with('plan_global', $plan_global);
    }

}
