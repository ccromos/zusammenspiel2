<?php

class UsuarioController extends BaseController {

    public function getIndex() {
        $usuarios = Usuario::all();
        return View::make('usuario.index')->with('usuarios', $usuarios);
    }

    public function getCrear() {
        return View::make('usuario.crear');
    }

    public function postCrear() {
        $usuario = new Usuario();
        $usuario->nombre = Input::get('nombre');
        $usuario->password = Input::get('password');
        $usuario->save();
        return Redirect::to('usuario');
    }

    public function getEliminar($id) {
        $usuario = Usuario::find($id);
        if (!is_null($usuario)) {
            $usuario->delete();
        }
        return Redirect::to('usuario');
    }

    public function getActualizar($id) {
        $usuario = Usuario::find($id);
        if (is_null($usuario)) {
            return Redirect::to('usuario');
        }
        return View::make('usuario.actualizar')->with('usuario', $usuario);
    }

    public function postActualizar($id) {
        $usuario = Usuario::find($id);
        if (!is_null($usuario)) {
            $usuario->nombre = Input::get('nombre');
            $usuario->password = Input::get('password');
            $usuario->save();
        }
        return Redirect::to('usuario');
    }

}

