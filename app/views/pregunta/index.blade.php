<h2>{{ $pregunta->titulo }}</h2>
<h3><a href="/foro/crear-pregunta">Responder</a></h3>
<p> {{ $pregunta->descripcion }}</p>
<ul>
@foreach($pregunta->respuestas as $respuesta)
        <li>{{ $respuesta->fecha_publicacion }} <br>
            {{ $respuesta->descripcion}}
        </li>
@endforeach
</ul>