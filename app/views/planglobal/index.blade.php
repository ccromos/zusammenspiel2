<h1>Plan Global</h1>
<h2> Identificacion</h2>
<ul>
    <li>Asignatura : {{ $plan_global->asignatura }} </li>
    <li>Codigo Sis : {{ $plan_global->codigo_sis }} </li>
    <li>Departamento : {{ $plan_global->departamento }} </li>
    <li>Docente : {{ $plan_global->docente }} </li>
    <li>Gestion : {{ $plan_global->gestion }} </li>
</ul>
<h2>Contenido Analitico</h2>
<ul>
@foreach($plan_global->temas as $tema)
        <li>{{ $tema-> nombre }} </li>
        <ul>
            @foreach($tema->subtemas as $subtema)
            <li>{{ $subtema->nombre }}</li>
            @endforeach
        </ul>
@endforeach    
</ul>
<h2>Bibliografia</h2>
<ul>
@foreach($plan_global->bibliografias as $bibliografia)
        <li>
            {{ $bibliografia-> titulo_libro }} <br>
            {{ $bibliografia-> autor }} <br>
            {{ $bibliografia-> edicion }} <br>        
        </li>
@endforeach
<ul>    