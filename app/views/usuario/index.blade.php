<h1>Usuarios</h1>
{{ HTML::link('usuario/crear','crear usuario') }}
@if($usuarios)
<ul>
    @foreach($usuarios as $usuario)
    <li>{{ $usuario->nombre}} - 
        {{ $usuario->password}} - 
        {{ HTML::link('usuario/actualizar/'.$usuario->id,'actualizar') }} -
        {{ HTML::link('usuario/eliminar/'.$usuario->id,'eliminar') }}
    </li>
    <ul>
        @foreach($usuario->telefonos as $telefono)
        <li>{{ $telefono-> numero }} </li>
        @endforeach
    </ul>
    @endforeach
</ul>
@else
No se tiene ni un usuario registrado
@endif