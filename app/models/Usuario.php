<?php

class Usuario extends Eloquent {

    protected $table = 'usuario';
    public $timestamps = false;

    public function telefonos() {
        return $this->hasMany('Telefono');
    }

}
