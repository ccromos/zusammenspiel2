<?php

class Tema extends Eloquent {

    protected $table = 'tema';
    public $timestamps = false;
    
    public function subtemas() {
        return $this->hasMany('Subtema');
    }
}

