<?php


class Respuesta extends Eloquent {
    protected $table = 'respuesta';
    public $timestamps = false;
	
	public function usuarios(){
        return $this->belongsTo('Usuario');
    }
}
