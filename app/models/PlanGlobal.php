<?php

class PlanGlobal extends Eloquent {

    protected $table = 'plan_global';
    public $timestamps = false;
    
    public function temas() {
        return $this->hasMany('Tema');
    }
    public function bibliografias() {
        return $this->hasMany('Bibliografia');
    }
}

