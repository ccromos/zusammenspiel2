<?php

class Pregunta extends Eloquent {

    protected $table = 'pregunta';
    public $timestamps = false;

    public function respuestas() {
        return $this->hasMany('Respuesta');
    }
    
	public function usuario()
    {
        return $this->belongsTo('Usuario');
    }
}
