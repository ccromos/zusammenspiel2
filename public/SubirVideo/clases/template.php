<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of template
 *
 * @author andre
 */
class template {
    private $temp;

    //constructor
    public function __construct() {
        $this->temp = implode("",file("site_media/html/plantilla.html"));
    }
    
        //esta funcion reemplaza en nombre por un archivo
    public function generar_template($nombre,$archivo)
    { 
	 $temp = $nombre;
	 $actm =implode(file($archivo));
	 $templateOUT = str_replace($temp, $actm, $this->temp); 
	 return $templateOUT;
    }	 
    
    //este reemplaza un nombre por un String en el parametro $fin y $fin es un String
    public function generar_template_texto($nombre,$texto,$fin)
    {
	 $temp = $nombre;
	 $actm = $texto;
	 $templateOUT = str_replace($temp, $actm, $fin); 

	 return $templateOUT;
    }
    
    //este reemplaza el nombre por un archivo de $fin que es un String
    public function generar_template2($nombre,$archivo,$fin)
    {
	 $temp = $nombre;
	 $actm =implode(file($archivo));
	 $templateOUT = str_replace($temp, $actm, $fin); 
	 return $templateOUT;
    }
    
    public function __destruct()
    {
        print "An object of type MyClass is being destroyed\n";
    }

    public function getTemplate(){
        return $this->temp;
    }
}

?>
